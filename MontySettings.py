import regex, collections, numpy, logging
from Settings import *
from MontyConfig import MontyConfig
from PyQt5 import QtGui, QtCore, QtWidgets


class MontySettings(QtWidgets.QDialog, Ui_settings):
    """
    Derives from Ui_reBuilder class built by Qt Designer
    """

    def __init__(self, parent=None):
        """
        Connects dialog events to functions
        """
        super(MontySettings, self).__init__(parent)
        self.setupUi(self)
        self.config = MontyConfig()

        self.buttonBox.button(QtWidgets.QDialogButtonBox.Apply).clicked.connect(self.Apply)

        self.radioBlacklist.toggled.connect(lambda: self.CheckState(self.radioBlacklist))
        self.radioOff.toggled.connect(lambda: self.CheckState(self.radioOff))
        self.radioWhitelist.toggled.connect(lambda: self.CheckState(self.radioWhitelist))

        self.listMode = self.config.ocrOptions["ListingMode"]

        self.Populate()

    def CheckState(self, radioButton):
        if radioButton.isChecked():
            self.listMode = radioButton.text()


    def Populate(self):
        """
        Loads settings from the config object into the settings UI.
        :return:
        """
        self.textEditSchema.clear()
        self.lineBlacklist.clear()
        self.lineWhitelist.clear()
        self.lineLanguages.clear()

        self.textEditSchema.setText(", ".join(self.config.schema))
        self.lineBlacklist.setText(self.config.ocrOptions["CharacterBlacklist"])
        self.lineWhitelist.setText(self.config.ocrOptions["CharacterWhitelist"])
        self.lineLanguages.setText(self.config.ocrOptions["Languages"])

        for child in self.groupListing.children():
            if type(child).__name__ == "QRadioButton":
                if child.text() == self.listMode:
                    child.setChecked(True)

    def Apply(self):
        """
        Apply settings changes to the config object.
        :return:
        """
        self.config.ocrOptions["CharacterBlacklist"] = self.lineBlacklist.text()
        self.config.ocrOptions["CharacterWhitelist"] = self.lineWhitelist.text()
        self.config.ocrOptions["ListingMode"] = self.listMode
        if regex.fullmatch("""\s*(.+?)(?:,|$)""", self.textEditSchema.toPlainText()):
            self.config.schema = regex.findall("""\s*(.+?)(?:,|$)""", self.textEditSchema.toPlainText())
            self.parent().BuildFields()
        if regex.fullmatch("""(?:[\p{L}-]+)(?:\+(?:[\p{L}-]+))*""", self.lineLanguages.text()):
            self.config.ocrOptions["Languages"] = self.lineLanguages.text()

        self.Populate()
        self.parent().UpdateUI()