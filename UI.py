import logging, sys, numpy, regex, configparser,csv

from MontyPage import MontyPage
from Monty import *
from PyQt5 import QtCore, QtGui, QtWidgets
from MontyConfig import MontyConfig
from RegexBuilder import RegexBuilder
from RegexManager import RegexManager
from MontySettings import MontySettings
from FieldRegexBuilder import FieldRegexBuilder
import QGraphicsSceneDecorator
import time
import cv2
from PIL import Image


class UI(QtWidgets.QMainWindow, Ui_MainWindow):
    """
    Derives from Ui_MainWindow class built by Qt Designer
    """
    def __init__(self, parent=None):
        """
        Connects UI events to functions, decorates Ui_MainWindow with QGraphicsSceneDecorator
        """
        super(UI, self).__init__(parent)
        self.setupUi(self)
        self.config = MontyConfig()
        self.currentImage = ""
        self.allImages = []
        self.imageData = {}
        self.curPixmap = None
        self.curNpImage = None
        self.currentSelectedEntryData = {}
        self.imageViewer = QGraphicsSceneDecorator.QGraphicsSceneDecorator(self)
        self.scanView.setScene(self.imageViewer)
        self.rightClickActionList = [self.actionDrawCNNFrame,self.actionCrop,self.actionDrawFrames]

        self.actionCurrentPageSave.triggered.connect(self.SaveCurrentAsCSV)
        self.actionAllPageSave.triggered.connect(self.SaveAllAsCSV)

        self.actionReparseBoxes.triggered.connect(self.ReparseBoxes)
        self.actionRotateLeft.triggered.connect(lambda: self.Rotate(direction = "left"))
        self.actionRotateRight.triggered.connect(lambda: self.Rotate(direction = "right"))

        self.actionCrop.triggered.connect(lambda: self.changeRightClickAction(self.actionCrop))
        self.actionDrawFrames.triggered.connect(lambda: self.changeRightClickAction(self.actionDrawFrames))
        self.actionDrawCNNFrame.triggered.connect(lambda: self.changeRightClickAction(self.actionDrawCNNFrame))

        self.menuBarFileOpen.triggered.connect(self.OpenImageFileDialog)
        self.menuBarViewZoomIn.triggered.connect(self.ZoomIn)
        self.menuBarViewZoomOut.triggered.connect(self.ZoomOut)
        self.menuBarViewZoomReset.triggered.connect (self.ZoomReset)
        self.actionScan.triggered.connect(self.ScanImage)
        self.actionScanAll.triggered.connect(self.ScanImages)
        self.actionExit.triggered.connect(self.close)
        self.entryPixelPadding.valueChanged.connect(self.DrawBoundingBoxes)
        self.minimumOCRConf.valueChanged.connect(self.DrawBoundingBoxes)
        self.scanImage.clicked.connect(self.actionScan.trigger)
        self.actionOpenFREBuilder.triggered.connect(lambda: self.OpenFieldRegexBuilder())
        self.actionOpenREBuilder.triggered.connect(lambda: self.OpenRegexBuilder())
        self.actionOpenREManager.triggered.connect(self.OpenRegexManager)
        self.actionGeneralSettings.triggered.connect(self.OpenMontySettings)
        self.buttonDeselect.clicked.connect(self.imageViewer.DeselectEntries)
        self.saveEntryData.clicked.connect(self.SaveEntryData)
        self.actionDeleteBoxes.triggered.connect(self.DeleteSelectedEntries)
        self.actionSelectAllBoxes.triggered.connect(self.SelectAllBoxes)

        self.actionSaveProject.triggered.connect(self.SaveProject)
        self.actionOpenProject.triggered.connect(self.OpenProject)

        self.entryRegex.currentTextChanged.connect(self.RegexTest)
        self.buttonRunRegex.clicked.connect(self.RegexTest)

        self.actionPreviousImage.triggered.connect(lambda: self.ChangePage(self.currentImageLoc - 1))
        self.actionNextImage.triggered.connect(lambda: self.ChangePage(self.currentImageLoc + 1))
        self.pageLeft.clicked.connect(self.actionPreviousImage.trigger)
        self.pageRight.clicked.connect(self.actionNextImage.trigger)

        self.tabs.currentChanged.connect(self.RefreshTab)
        self.minimumOCRConfTable.valueChanged.connect(self.RefreshTab)

        self.selectImage.currentTextChanged.connect(lambda : self.ChangePage(self.selectImage.currentIndex()))

        self.curImageFile = None
        self.currentImageLoc = None

        self.BuildFields()

    def changeRightClickAction(self,curAction):
        for action in self.rightClickActionList:
            if not action == curAction:
                action.setChecked(False)

    def Rotate(self,direction=None):

        for rect in self.imageViewer.items:
            self.imageViewer.removeItem(rect)
        self.imageViewer.items.clear()
        self.imageViewer.clear()

        if direction:
            if direction == "left":
                self.imageData[self.currentImage].currentRotation += .2
                rotation = 1
            else:
                self.imageData[self.currentImage].currentRotation -= .2
                rotation = -1
        if self.curNpImage is not None:
            img = self.curNpImage
            if direction:
                rotation *= .2
            else:
                rotation = 0
        else:
            img = cv2.imread(self.currentImage)
            rotation = self.imageData[self.currentImage].currentRotation

        rows,cols = (img.shape[0],img.shape[1])
        M = cv2.getRotationMatrix2D((cols/2,rows/2),rotation,1)
        img = cv2.warpAffine(img,M,(cols,rows))
        self.curNpImage = img
        img = img[:, :, ::-1].copy()

        height, width, channel = img.shape
        bytesPerLine = 3 * width
        qImg = QtGui.QImage(img.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888)
        self.curPixmap = QtGui.QPixmap.fromImage(qImg)
        self.curImageFile = self.imageViewer.addPixmap(self.curPixmap)
        self.DrawBoundingBoxes()
        self.UpdateUI()
        pilImg = Image.fromarray(img)

        return pilImg

    def Crop(self,topLeft,bottomRight,fromChangePage=False):
        """
        Crop the image
        :return:none
        """
        for rect in self.imageViewer.items:
            self.imageViewer.removeItem(rect)
        self.imageViewer.items.clear()
        self.imageViewer.clear()

        if fromChangePage:
            self.imageData[self.currentImage].topLeft = topLeft
            self.imageData[self.currentImage].bottomRight = bottomRight
        elif self.imageData[self.currentImage].topLeft is None:
            rotation = self.imageData[self.currentImage].currentRotation
            self.imageData[self.currentImage] = MontyPage(image=self.currentImage,
                                                              pixelPadding=self.entryPixelPadding.value())
            self.imageData[self.currentImage].topLeft = topLeft
            self.imageData[self.currentImage].bottomRight = bottomRight
            self.imageData[self.currentImage].currentRotation = rotation
        else:
            newTopX = self.imageData[self.currentImage].topLeft[0] + topLeft[0]
            newTopY = self.imageData[self.currentImage].topLeft[1] + topLeft[1]
            newBottomX = self.imageData[self.currentImage].topLeft[0] + bottomRight[0]
            newBottomY = self.imageData[self.currentImage].topLeft[1] + bottomRight[1]
            rotation = self.imageData[self.currentImage].currentRotation
            self.imageData[self.currentImage] = MontyPage(image=self.currentImage,
                                                              pixelPadding=self.entryPixelPadding.value())
            self.imageData[self.currentImage].topLeft = (newTopX,newTopY)
            self.imageData[self.currentImage].bottomRight = (newBottomX, newBottomY)
            self.imageData[self.currentImage].currentRotation = rotation
        if self.curNpImage is not None:
            img = self.curNpImage
        else:
            img = cv2.imread(self.currentImage)
        cropImg = img[topLeft[1]:bottomRight[1], topLeft[0]:bottomRight[0]]
        self.curNpImage = cropImg
        cropImg = cropImg[:, :, ::-1].copy()
        height, width, channel = cropImg.shape
        bytesPerLine = 3 * width
        qImg = QtGui.QImage(cropImg.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888)
        self.curPixmap = QtGui.QPixmap.fromImage(qImg)
        self.curImageFile = self.imageViewer.addPixmap(self.curPixmap)

    def SaveCurrentAsCSV(self):
        if self.imageData:
            if self.imageData[self.currentImage].matchedEntries:
                path = QtWidgets.QFileDialog.getSaveFileName(self,'Apply File', self.currentImage + '.csv', 'CSV(*.csv)')
                entries = self.imageData[self.currentImage].matchedEntries
                self.SaveAsCSV(path[0], entries)
            else:
                self.DisplayErrorMsg("This page has no entries")
        else:
            self.DisplayErrorMsg("No Files Loaded In")

    def SaveAllAsCSV(self):
        path = QtWidgets.QFileDialog.getSaveFileName(self, 'Apply File', 'untitled.csv', 'CSV(*.csv)')
        entries = [entry for imageName, ocrData in self.imageData.items() for entry in ocrData.matchedEntries]
        self.SaveAsCSV(path[0], entries)

    def SaveAsCSV(self, path=None, entries=None):
        if path:
            if entries:
                with open(path, 'w') as stream:
                    writer = csv.DictWriter(stream, self.config.schema+["Image", "X", "Y", "W", "H"])
                    writer.writeheader()
                    for curEntry in entries:
                        try:
                            line = curEntry.parsedText
                            line["Image"] = curEntry.image
                            line["X"] = str(curEntry.box["x"])+"/"+ str(curEntry.imageSize["X"])
                            line["Y"] = str(curEntry.box["y"]) + "/" + str(curEntry.imageSize["Y"])
                            line["W"] = curEntry.box["w"]
                            line["H"] = curEntry.box["h"]
                            writer.writerow(line)
                        except Exception as e:
                            logging.debug("""Adding line, "{}" to CSV failed. Error: {}""".format(curEntry.rawText, e))

    def OpenProject(self):
        file, _ = QtWidgets.QFileDialog.getOpenFileNames(self, 'Choose a configuration file...', 'Project.cfg', '')
        if file:
            self.config.LoadConfig(file)
        self.BuildFields()
        self.UpdateUI()

    def SaveProject(self):
        file, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Apply configuration file as...', 'Project.cfg', '')
        if file:
            self.config.SaveConfig(file)

    def ChangePage(self, nextPage):
        """
        Changes page shown in Page View and Table View (page).
        :param nextPage:
        :return:
        """
        if self.allImages:
            self.pageRight.setEnabled(False)
            self.pageLeft.setEnabled(False)
            self.actionPreviousImage.setEnabled(False)
            self.actionNextImage.setEnabled(False)
            for rect in self.imageViewer.items:
                self.imageViewer.removeItem(rect)
            self.imageViewer.items.clear()
            self.imageViewer.clear()
            self.currentImageLoc = nextPage
            self.currentImage = self.allImages[nextPage]
            self.selectImage.setCurrentIndex(nextPage) #making sure the select image is on the right index too
            self.curNpImage = None
            if self.currentImage in self.imageData:
                if not self.imageData[self.currentImage].currentRotation == 0:
                    self.Rotate()
                if self.imageData[self.currentImage].topLeft is not None:
                    self.Crop(self.imageData[self.currentImage].topLeft, self.imageData[self.currentImage].bottomRight,
                              fromChangePage=True)
                if self.curNpImage is None:
                    self.curPixmap = QtGui.QPixmap.fromImage(QtGui.QImage(self.currentImage))
                    self.curImageFile = self.imageViewer.addPixmap(self.curPixmap)
                self.scanView.fitInView(self.imageViewer.sceneRect(), QtCore.Qt.KeepAspectRatio)
                self.DrawBoundingBoxes()
            else:
                self.imageData[self.currentImage] = MontyPage(image=self.currentImage,
                                                              pixelPadding=self.entryPixelPadding.value())
                self.curPixmap = QtGui.QPixmap.fromImage(QtGui.QImage(self.currentImage))
                self.curImageFile = self.imageViewer.addPixmap(self.curPixmap)
                self.scanView.fitInView(self.imageViewer.sceneRect(), QtCore.Qt.KeepAspectRatio)
            self.ScanImageEnable(True)

            if nextPage != 0:
                self.pageLeft.setEnabled(True)
                self.actionPreviousImage.setEnabled(True)
            if nextPage != len(self.allImages) - 1:
                self.pageRight.setEnabled(True)
                self.actionNextImage.setEnabled(True)
        self.UpdateUI()

    def RefreshTab(self):
        """
        Refreshes currently selected tab.
        :return:
        """
        try:
            current = self.tabs.currentIndex()
            if current == 1 and self.imageData[self.currentImage]:
                self.PopulateTable(entries = self.imageData[self.currentImage].matchedEntries, minConf = self.minimumOCRConf.value())
            elif current == 2:
                self.PopulateTable(table = self.tableAll, entries = [entry for ocrData in self.imageData.values() for entry in ocrData.matchedEntries], minConf = self.minimumOCRConfTable.value())
            else:
                selectedIndexSet = set()
                for idx in self.tablePage.selectedIndexes():
                    selectedIndexSet.add(idx.row())
                self.imageViewer.DeselectEntries()
                for i in selectedIndexSet:
                    try:
                        self.tablePage.item(i,0).data(QtCore.Qt.UserRole).selected = True
                    except:
                        logging.debug("Error in RefreshTab selecting page enties")
                self.DrawBoundingBoxes()
                self.PopulateEntryDataBoxes()
                self.UpdateUI()
        except Exception as e:
            logging.debug("RefreshTab failed: {}".format(e))

    def OpenImageFileDialog(self):
        """
        Allows the user to select what images they would like to open.
        :return:
        """
        images, _ = QtWidgets.QFileDialog.getOpenFileNames(self, 'Choose an Image', '', 'Image File (*.jpg)')

        if images:
            self.selectImage.clear()
            self.actionAppliedRegexes.setEnabled(True)
            self.ScanImageEnable(True)
            self.actionPreviousImage.setEnabled(True)
            self.actionNextImage.setEnabled(True)
            self.actionDeleteBoxes.setEnabled(True)
            self.actionReparseBoxes.setEnabled(True)
            self.actionSelectAllBoxes.setEnabled(True)
            self.actionRotateLeft.setEnabled(True)
            self.actionRotateRight.setEnabled(True)
            self.actionCrop.setEnabled(True)
            self.allImages = images
            self.entryPixelPadding.setDisabled(False)
            self.actionCurrentPageSave.setEnabled(True)
            self.actionAllPageSave.setEnabled(True)
            self.actionSaveProject.setEnabled(True)
            for i in self.allImages:
                self.selectImage.addItem(QtCore.QFileInfo(i).fileName())#adding the filenames to selectImage widget
                if i not in self.imageData:
                    self.imageData[i] = MontyPage(image=i, pixelPadding=self.entryPixelPadding.value())
            self.selectImage.setEnabled(True)
            self.minimumOCRConf.setDisabled(False)
            self.ChangePage(0)

    def OpenFieldRegexBuilder(self, text=None):
        """
        Opens FieldRegexBuilder.
        :param text:
        :param regex:
        :return:
        """
        if text == None:
            text = self.entryText.toPlainText()
        self.freBuilder = FieldRegexBuilder(parent=self, text=text)
        self.freBuilder.show()

    def OpenRegexBuilder(self, text=None, regex=None):
        """
        Opens RegexBuilder and passes the current text and Regex
        :return:
        """
        if text == None:
            text = self.entryText.toPlainText()
        if regex == None:
            regex = self.entryRegex.currentData()

        if regex:
            self.reBuilder = RegexBuilder(parent=self, text=text, reg=regex)
        else:
            self.reBuilder = RegexBuilder(parent=self, text=text)
        self.reBuilder.show()

    def OpenRegexManager(self):
        """
        Opens RegexManager
        :return:
        """
        self.reManager = RegexManager(parent=self)
        self.reManager.show()

    def OpenMontySettings(self):
        """
        Opens MontySettings
        :return:
        """
        self.settings = MontySettings(parent=self)
        self.settings.show()

    def RegexTest(self):
        """
        Tests RegEx on the current text, then displays results.
        :return:
        """
        if len(self.entryRegex.currentText()) > 1 and len(self.entryText.toPlainText()) > 1:
            for field in self.config.schema:
                self.fieldEditBoxes[field].clear()
            match = self.entryRegex.currentData().search(self.entryText.toPlainText())
            if match:
                self.PopulateEntryDataBoxes(parsedData = match.groupdict())


    def PopulateEntryDataBoxes(self, parsedData=None):
        """
        Populates the entry data boxes with the text, RegEx, and the data for each field, if available
        :param entry:
        :return:
        """
        for field in self.config.schema:
            self.fieldEditBoxes[field].clear()

        if parsedData:
            for field in self.config.schema:
                try:
                    self.fieldEditBoxes[field].setEditText(parsedData[field])
                except:
                    self.fieldEditBoxes[field].setEditText("")
        else:
            self.entryText.clear()
            self.entryRegex.clear()

            allEntries = list(set(self.imageData[self.currentImage].matchedEntries) | set(self.imageData[self.currentImage].unmatchedEntries))

            entries = [entry for entry in allEntries if entry.selected == True]
            self.entryCount.setText(str(len(entries)) + " entries selected.")
            if len(entries) > 0:
                self.entryConfidence.setText("Avg. OCR Confidence for selected: " + "%.2f" % numpy.mean([entry.confidence for entry in entries]))
                self.buttonRunRegex.setEnabled(True)
                self.saveEntryData.setEnabled(True)

                for entry in entries:
                    self.entryText.appendPlainText(entry.rawText)
                self.entryText.moveCursor(QtGui.QTextCursor.Start)  # set cursor/scroll bar to the top
                for rege in [self.config.regexes[guid] for guid in self.config.regexes.keys() if guid in set(
                        entry.regex for entry in entries if entry.regex)]:  # put the regexes used on top
                    self.entryRegex.addItem("{}: {}".format(rege.name, rege.pattern.replace("\n", "")), rege)
                for rege in [self.config.regexes[guid] for guid in self.config.regexes.keys() if guid not in set(
                        entry.regex for entry in entries if entry.regex)]:  # add the rest of the regexes
                    self.entryRegex.addItem("{}: {}".format(rege.name, rege.pattern.replace("\n", "")), rege)
            else:
                for rege in [self.config.regexes[guid] for guid in self.config.regexes.keys()]:  # put the regexes used on top
                    self.entryRegex.addItem("{}: {}".format(rege.name, rege.pattern.replace("\n", "")), rege)
                self.entryConfidence.setText("")
                self.buttonRunRegex.setEnabled(False)
                self.saveEntryData.setEnabled(False)


            self.currentSelectedEntryData.clear()
            for field in self.config.schema:
                values = [entry.parsedText[field] for entry in entries if entry.match and field in entry.parsedText]
                valuesSet = set(values)
                if len(valuesSet) > 1:
                    self.fieldEditBoxes[field].addItem("Varying values")
                    self.fieldEditBoxes[field].addItems(list(valuesSet))
                    self.fieldEditBoxes[field].addItem("")
                elif len(valuesSet) == 1:
                    self.fieldEditBoxes[field].addItem(values[0])
                    self.fieldEditBoxes[field].addItem("")
                self.currentSelectedEntryData[field] = self.fieldEditBoxes[field].currentText()

    def ReparseBoxes(self):
        self.imageData[self.currentImage].ParseLines()
        self.UpdateUI()

    def SaveEntryData(self):
        """
        Saves modified data for currently selected entries.
        :return:
        """
        entries = [entry for entry in list(
                                set([item for item in self.imageData[self.currentImage].matchedEntries]) | set(
                                [item for item in self.imageData[self.currentImage].unmatchedEntries])) if
                              entry.selected == True]
        if len(entries) == 1:
            entries[0].rawText = self.entryText.toPlainText()
            entries[0].regex = self.entryRegex.currentData().guid

        for entry in entries:
            if entry in self.imageData[self.currentImage].unmatchedEntries:
                entry.match = True
                self.imageData[self.currentImage].matchedEntries.append(entry)
                self.imageData[self.currentImage].unmatchedEntries.remove(entry)

            for field in self.config.schema:  # Set parsed data
                if self.currentSelectedEntryData[field] != self.fieldEditBoxes[field].currentText():
                    entry.parsedText[field] = self.fieldEditBoxes[field].currentText()
                    entry.userModified["fields"] = True

        self.PopulateEntryDataBoxes()


    def DeleteEntry(self, entry):
        """
        Removes an entry from the matchedEntries or unmatchedEntries list
        :param entry:
        :return:
        """
        if entry in self.imageData[self.currentImage].matchedEntries:
            self.imageData[self.currentImage].matchedEntries.remove(entry)
        elif entry in self.imageData[self.currentImage].unmatchedEntries:
            self.imageData[self.currentImage].unmatchedEntries.remove(entry)

    def DeleteSelectedEntries(self):
        """
        Deletes entries that are marked as selected.
        :param entry:
        :return:
        """
        entries = [entry for entry in list(set(self.imageData[self.currentImage].matchedEntries) | set(self.imageData[self.currentImage].unmatchedEntries)) if entry.selected == True]

        deleteConfirm = QtWidgets.QMessageBox.question(self, "Confirm Deletion", "Are you sure you want to delete {} entries?".format(len(entries)),
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)

        if deleteConfirm == QtWidgets.QMessageBox.Yes:
            [self.DeleteEntry(entry) for entry in entries]

        self.UpdateUI()

    def SelectAllBoxes(self):
        """
        Selects all boxes in the view
        """
        for entry in list(set(self.imageData[self.currentImage].matchedEntries) | set(self.imageData[self.currentImage].unmatchedEntries)):
            entry.selected = True

        self.UpdateUI()

    def DrawBoundingBoxes(self):
        """
        Draws or redraws the bounding boxes for all matched entries
        :return:
        """
        pixelPadding = self.entryPixelPadding.value()
        minConf = self.minimumOCRConf.value()

        for rect in self.imageViewer.items:
            self.imageViewer.removeItem(rect)
        self.imageViewer.items.clear()

        entries = list(set(self.imageData[self.currentImage].matchedEntries) | set(self.imageData[self.currentImage].unmatchedEntries)) # union of the unmatched and unmatched entries

        for entry in entries:
            self.DrawEntry(entry = entry, pixelPadding = pixelPadding, minConf = minConf)

        self.imageData[self.currentImage].pixelPadding = pixelPadding
        self.imageData[self.currentImage].minimumOCRConfidence = minConf

    def DrawEntry(self, entry, pixelPadding, minConf):
        """
        Draws an entry at the specified pixelPadding and minConf. Does not remove old box if the entry has already been drawn.
        """
        if entry.autoDefined:
            entryBox = QtCore.QRectF(entry.box['x'] - pixelPadding, entry.box['y'] - pixelPadding,
                                     entry.box['w'] + pixelPadding, entry.box['h'] + pixelPadding)
        else:
            entryBox = QtCore.QRectF(entry.box['x'], entry.box['y'], entry.box['w'], entry.box['h'])
        # Draw unselected matched entry
        if not entry.selected and entry.match:
            newBox = self.imageViewer.addRect(entryBox, QtGui.QPen(QtGui.QColor('black')),
                                              QtGui.QBrush(
                                                  QtGui.QColor.fromHsv(
                                                      (entry.confidence - minConf) * (120 / (
                                                          100 - minConf)) if entry.confidence - minConf > 0 else 0,
                                                      255, 160, 120),
                                                  QtCore.Qt.SolidPattern))
        # Draw unselected and unmatched entry
        elif not entry.selected and not entry.match:
            newBox = self.imageViewer.addRect(entryBox, QtGui.QPen(QtGui.QColor('black')),
                                              QtGui.QBrush(
                                                  QtGui.QColor(238, 130, 238, 120),
                                                  QtCore.Qt.SolidPattern))
        # Draw selected entry
        elif entry.selected:
            newBox = self.imageViewer.addRect(entryBox, QtGui.QPen(QtGui.QColor('black')),
                                              QtGui.QBrush(QtGui.QColor.fromHsv(171, 255, 160, 120),
                                                           QtCore.Qt.SolidPattern))
        # Attach entry to item
        newBox.entry = entry
        self.imageViewer.items.append(newBox)

    def ScanImage(self, fileName = False): # .connect passes in False, so it's best as the default
        """
        Scans the current image using tesserocr, then parses the lines found.
        :param fileName:
        :return:
        """
        if not (fileName == False):
            self.currentImage = fileName
        self.entryCount.setText("Scanning Page")
        self.ScanImageEnable(False)
        for rect in self.imageViewer.items:
            self.imageViewer.removeItem(rect)
        self.imageViewer.items.clear()
        self.repaint()
        if self.currentImage not in self.imageData:
            self.imageData[self.currentImage] = MontyPage(image=self.currentImage,
                                                          pixelPadding=self.entryPixelPadding.value())
        if self.curNpImage is None:
            img = Image.open(self.currentImage)
        else:
            img = Image.fromarray(self.curNpImage)
        self.imageData[self.currentImage].OCRFrame(imageFrame=img)
        self.imageData[self.currentImage].ParseLines()

        self.UpdateUI()

        self.ScanImageEnable(True)

    def ScanImages(self):
        """
        Scans all opened images.
        :return:
        """
        if self.allImages:
            curPage = self.currentImageLoc
            for page, _ in enumerate(self.allImages):
                self.ChangePage(page)
                self.ScanImage()
            self.ChangePage(curPage)


    def ScanImageEnable(self, isEnabled):
        """
        Set all ways to scan to disabled or enabled (True/False).
        """
        self.scanImage.setEnabled(isEnabled)
        self.actionScan.setEnabled(isEnabled)
        self.actionScanAll.setEnabled(isEnabled)

    def UpdateUI(self):
        """
        Updates mean, confidence, minimum OCR confidence, and draws boxes
        """
        try:
            self.imageData[self.currentImage].minimumOCRConfidence = self.minimumOCRConf.value()
            if (self.imageData[self.currentImage].matchedEntries):
                self.entryCount.setText(str(len(self.imageData[self.currentImage].matchedEntries)) + " matching entries found.")
                confidences = [entry.confidence for entry in self.imageData[self.currentImage].matchedEntries]
                if len(confidences) > 1:
                    meanConf = numpy.mean(confidences)
                    stdConf = numpy.std(confidences)
                    self.ocrStats.setText("Overall OCR confidence: Avg = " + "%.2f" % meanConf + ", Std. Dev. = " + "%.2f" % stdConf)
                    if self.autoSetConf.checkState():
                        #This will cause the boxes to be redrawn, since the value is being changed
                        self.minimumOCRConf.setValue(int(meanConf - 10 * stdConf))
                else:
                    self.ocrStats.setText("")
                    if self.autoSetConf.checkState():
                        #This will cause the boxes to be redrawn, since the value is being changed
                        self.minimumOCRConf.setValue(75)
            else:
                self.PopulateEntryDataBoxes()
                self.ocrStats.setText("")
                self.entryCount.setText("")

            # Draw the boxes if they aren't drawn from minimumOCRConf being changed
            if self.imageData[self.currentImage].minimumOCRConfidence == self.minimumOCRConf.value():
                self.PopulateEntryDataBoxes()
                self.DrawBoundingBoxes()
        except Exception as e:
            logging.debug("UpdateUI failed. There probably isn't an image loaded. Error: {}".format(e))

    def PopulateTableRow(self, table, row, entry, minConf):
        """
        Populates a row in the passed table with color based on minConf.
        :param table:
        :param row:
        :param entry:
        :param minConf:
        :return:
        """
        for col, field in enumerate(self.config.schema):
            try:
                tableItem = QtWidgets.QTableWidgetItem(entry.parsedText[field])
                tableItem.setData(QtCore.Qt.UserRole, entry)
                table.setItem(row, col, tableItem)

            except:
                tableItem = QtWidgets.QTableWidgetItem("")
                table.setItem(row, col, tableItem)
            try:
                if entry.selected:
                    table.setSelectionMode(3)
                    tableItem.setSelected(True)
                table.item(row, col).setBackground(QtGui.QColor.fromHsv((entry.confidence - minConf) * (120 / (100 - minConf)) if entry.confidence - minConf > 0 else 0, 255, 160, 120))

            except:
                None

    def PopulateTable(self, table = None, minConf = None, entries = None):
        """
        Populates a table with entries, coloring individual rows by the minimum confidence.
        :param table:
        :param minConf:
        :param entries:
        :return:
        """
        if table is None:
            table = self.tablePage
        if minConf is None:
            minConf = self.minimumOCRConf.value()
        if entries is None:
            entries = self.imageData[self.currentImage].matchedEntries
        table.clear()
        table.setColumnCount(len(self.config.schema))
        table.setRowCount(len(entries))
        table.setHorizontalHeaderLabels(self.config.schema)

        for row, entry in enumerate(entries):
            self.PopulateTableRow(table = table, row = row, entry = entry, minConf = minConf)

    def ScanFrame(self,curFrame,offset):
        """
        Scans a frame with tesserocr and parses its data.
        :param curFrame:
        :param offset:
        :return:
        """
        self.entryCount.setText("Scanning...")
        self.ScanImageEnable(False)
        self.repaint()
        if self.currentImage not in self.imageData:
            self.imageData[self.currentImage] = MontyPage(image=self.currentImage,
                                                          pixelPadding=self.entryPixelPadding.value())
        self.imageData[self.currentImage].OCRFrame(imageFrame=curFrame,offset=offset, frameScan=True)
        self.imageData[self.currentImage].ParseLines(frameScan=True)

        self.UpdateUI()

        self.ScanImageEnable(True)

    def BuildFields(self):
        """
        Create new field view based on the SCHEMA
        :return:
        """
        scrollAreaLayout = QtWidgets.QVBoxLayout()

        scrollWidget = QtWidgets.QWidget()
        scrollWidget.setLayout(scrollAreaLayout)

        self.scrollFields.setWidget(scrollWidget)

        self.fieldEditBoxes = {}
        for field in self.config.schema:
            fieldLabel = QtWidgets.QLabel(field)
            scrollAreaLayout.addWidget(fieldLabel)
            fieldEdit = QtWidgets.QComboBox()
            fieldEdit.setEditable(True)
            self.fieldEditBoxes[field] = fieldEdit
            scrollAreaLayout.addWidget(fieldEdit)

    def ZoomIn(self):
        """
        Scales view by 1.2
        """
        self.scanView.scale(1.2, 1.2)

    def ZoomOut(self):
        """
        Scales view by 1/1.2
        """
        self.scanView.scale(1 / 1.2, 1 / 1.2)

    def ZoomReset(self):
        """
        Resets zoom to default
        """
        self.scanView.fitInView(QtCore.QRectF(0, 0, self.geometry().width() * 3, self.geometry().height() * 3), QtCore.Qt.KeepAspectRatio)

    def closeEvent(self, QCloseEvent):
        """
        Overrides closeEvent method so confirmation is needed to close.
        :param QCloseEvent:
        :return:
        """
        exitDialog = QtWidgets.QMessageBox.question(self, "Exit", "Are you sure you want to quit?",
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)

        if exitDialog == QtWidgets.QMessageBox.Yes:
            QCloseEvent.accept()
            logging.info("Exit application")
        else:
            QCloseEvent.ignore()

    def DisplayErrorMsg(self, text="Insert Error String"):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Warning)
        msgBox.setText(text)

        msgBox.exec_()
