import logging, sys, os
from Monty import *
import UI
import time

if __name__ == '__main__':
    os.chdir(sys.path[0])
    os.makedirs("logs", exist_ok=True)
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s',
                        filename="logs/Monty{}.log".format(time.strftime("%d%m%Y-%H%M%S")))
    logging.getLogger().addHandler(logging.StreamHandler()) #TODO: Make this so it only prints out to the console if enabled in the config file.
    logging.info('Started Monty')
    os.chdir(sys.path[0])
    app = QtWidgets.QApplication(sys.argv)
    tbdexWindow = UI.UI()
    tbdexWindow.show()
    logging.info("UI initialized.")
    sys.exit(app.exec_())