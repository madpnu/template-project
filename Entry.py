class Entry():
    def __init__(self, box = {'x': 0, 'y': 0, 'w': 0, 'h': 0, 'x2': 0, 'y2': 0, 'xMid': 0, 'yMid': 0}, confidence = -1, rawText ="", rawPos = -1, regex=None, match = None, autoDefined = True, selected = False, parsedText = {}, image = "", imageSize = {"X": 0, "Y": 0}):
        self.box = box
        self.confidence = confidence
        self.rawText = rawText
        self.rawPos = rawPos
        self.regex = regex
        self.match = match
        self.autoDefined = autoDefined
        self.parsedText = parsedText
        self.userModified = {"text" : False, "regex" : False, "fields" : False, "parsed" : False }
        self.selected = selected
        self.image = image
        self.imageSize = imageSize

    def MergeEntry(self, other):
        """
        Merge bounding boxes, average confidence and concatenate text
        :param other:
        :return:
        """
        if other.rawText:
            self.rawText = self.rawText + other.rawText
            self.confidence = (self.confidence + other.confidence) / 2
            self.box['x2'] = self.box['w'] + self.box['x'] if self.box['w'] + self.box['x'] > other.box['w'] + other.box['x'] else other.box['w'] + other.box['x']
            self.box['y2'] = self.box['h'] + self.box['y'] if self.box['h'] + self.box['y'] > other.box['h'] + other.box['y'] else other.box['h'] + other.box['y']

            self.box['x'] = self.box['x'] if self.box['x'] < other.box['x'] else other.box['x']
            self.box['y'] = self.box['y'] if self.box['y'] < other.box['y'] else other.box['y']
            self.box['w'] = self.box['x2'] - self.box['x']
            self.box['h'] = self.box['y2'] - self.box['y']
            self.box['xMid'] = self.box['x'] + self.box['w']/ 2
            self.box['yMid'] = self.box['y'] + self.box['h']/ 2

