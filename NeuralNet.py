import logging, sys, numpy, time
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
import cv2
from PyQt5 import QtCore


class NeuralNet:

    def __init__(self):
        # dimensions of our images.
        self.imgWidth,self.imgHeight = 150, 150

        self.weightsPath = "./april13.h5"

        self.model = Sequential()
        self.model.add(Convolution2D(32, 3, 3, input_shape=(self.imgWidth, self.imgHeight,3)))
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Convolution2D(32, 3, 3))
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Convolution2D(64, 3, 3))
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Flatten())
        self.model.add(Dense(64))
        self.model.add(Activation('relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(5))
        self.model.add(Activation('sigmoid'))

        self.model.load_weights(self.weightsPath)

    def Search(self,img,winWW,winHH,offsetX,offsetY,yBuffer):
        numEntriesFound = 0
        start = time.clock()
        entries = []
        img = numpy.array(img)
        img = img[:, :, ::-1].copy()
        imgCopy = img.copy()
        xBuffer = 0
        img2gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        img2gray = cv2.GaussianBlur(img2gray,(5,5),0)
        ret,mask = cv2.threshold(img2gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        imageFinal = cv2.bitwise_and(img2gray,img2gray,mask = mask)
        imageFinal = cv2.GaussianBlur(imageFinal,(5,5),0)
        ret,newImg = cv2.threshold(imageFinal,180,255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)  # for black text , cv.THRESH_BINARY_INV
        kernel = cv2.getStructuringElement(cv2.MORPH_CROSS,(3 , 3)) # to manipulate the orientation of dilution , large x means horizonatally dilating  more, large y means vertically dilating more
        dilated = cv2.dilate(newImg,kernel,iterations = 9) # dilate , more the iteration more the dilation

        _,contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE) # get contours

        for contour in contours:
            # get rectangle bounding contour
            [x,y,w,h] = cv2.boundingRect(contour)

            # Don't plot small false positives that aren't text
            if w < 30 and h<30:
                continue

            if x + winWW + xBuffer < img.shape[1] and y + winHH + yBuffer < img.shape[0] and x - xBuffer > 0 and y - yBuffer > 0:
                x -= xBuffer
                y -= yBuffer
                winH = winHH+(yBuffer)
                winW = winWW+(2*xBuffer)
            else:
                continue

            cropImg = imgCopy[y:y+winH,x:x+winW]
            cropImg = cv2.cvtColor(cropImg, cv2.COLOR_BGR2RGB)
            cropImg = numpy.true_divide(cropImg,255)
            cropImg = cv2.resize(cropImg, (self.imgWidth,self.imgHeight))
            cropImg = numpy.expand_dims(cropImg, axis=0)
            prediction = self.model.predict(cropImg)
            prediction = prediction.ravel()
            if numpy.argmax(prediction) != 4 and prediction[numpy.argmax(prediction)] > .000001:
                numEntriesFound+=1
                # draw rectangle around contour on original image
                entryBox = QtCore.QRectF(offsetX+x,offsetY+y,winW,winH)
                entries.append(entryBox)
                #cv2.rectangle(img,(x,y),(x+winW,y+winH),(255,0,255),2)

        #cv2.imwrite("boxes.png",img)
        logging.debug("Search Took: "+ str(time.clock() - start) + "s")
        logging.debug("Number of Entries Found: " + str(numEntriesFound))
        return(entries)