from PyQt5 import QtCore, QtGui, QtWidgets


class QGraphicsViewDecorator(QtWidgets.QGraphicsView):
    """
    Decorator class for QGraphicsView
    """
    def __init__(self, parent=None):
        super(QGraphicsViewDecorator, self).__init__(parent)

    def resizeEvent(self, event):
        self.scanView.fitInView(self.sceneRect(), QtCore.Qt.KeepAspectRatio)