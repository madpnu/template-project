import collections
import configparser
import logging

import regex

from MontyRegex import MontyRegex
from Singleton import Singleton


class MontyConfig(metaclass=Singleton):
    def __init__(self):
        self.configParser = None
        self.schema = []
        self.genReg = []
        self.ocrOptions = collections.OrderedDict()
        self.fieldRegexes = collections.OrderedDict()
        self.regexes = collections.OrderedDict()
        self.regexOptions = collections.OrderedDict()
        self.LoadConfig()

    def __getitem__(self, key):
        return self.configParser[key]

    def __setitem__(self, key, value):
        self.configParser[key] = value

    def LoadConfig(self, file = 'Project.cfg'):
        """
        Loads a configuration file.
        :return:
        """
        try:
            # Create raw config parser so it can be set to read in keys, respecting case.
            self.configParser = configparser.RawConfigParser(dict_type=collections.OrderedDict)
            # Set case-sensitive parser by overriding the method that would oridinarily make the keys lowercase.
            self.configParser.optionxform = str
            self.configParser.read(file)

            # Turn config file into dictionaries
            self.schema = regex.findall("""\s*(.+?)(?:,|$)""", self.configParser['GENERAL']['Schema'])
            self.ocrOptions = {key: self.configParser['OCR'][key] for key in self.configParser['OCR'].keys()}
            self.fieldRegexes = {key: self.configParser['FIELD REGEXES'][key] for key in
                                 self.configParser['FIELD REGEXES'].keys()}

            # Parse raw regexes
            rawRegexesDict = {key: self.configParser['RAW REGEXES'][key] for key in
                              self.configParser['RAW REGEXES'].keys()}
            for guid in set([key.split('_', 1)[0] for key in rawRegexesDict.keys()]):
                try:
                    self.regexes[guid] = MontyRegex(guid=guid, name=rawRegexesDict[guid + "_name"], isAuto=False,
                                                    pattern=rawRegexesDict[guid + "_pattern"],
                                                    flags=rawRegexesDict[guid + "_flags"].replace(" ", "").split(","),
                                                    schema=self.schema, fieldRegexes=self.fieldRegexes)
                    self.regexes[guid].Compile()
                except Exception as e:
                    logging.debug("Error parsing raw regex from config file: {}".format(e))

            # Parse auto regexes
            autoRegexesDict = {key: self.configParser['AUTO REGEXES'][key] for key in
                               self.configParser['AUTO REGEXES'].keys()}
            for guid in set([key.split('_', 1)[0] for key in autoRegexesDict.keys()]):
                try:
                    self.regexes[guid] = MontyRegex(guid=guid, name=autoRegexesDict[guid + "_name"], isAuto=True,
                                                    pattern=autoRegexesDict[guid + "_pattern"],
                                                    flags=autoRegexesDict[guid + "_flags"].replace(" ", "").split(","),
                                                    schema=self.schema, fieldRegexes=self.fieldRegexes)
                    self.regexes[guid].Compile()
                except Exception as e:
                    logging.debug("Error parsing auto regex from config file: {}".format(e))

            self.regexOptions = {key: self.configParser['REGEX OPTIONS'][key] for key in
                                 self.configParser['REGEX OPTIONS'].keys()}
            self.genReg = list(self.regexes.keys())

        except Exception as e:
            logging.debug("Config load failed: {}".format(e))

    def SaveConfig(self, file):
        """
        Saves the current configuration to the disk.
        :return:
        """
        # Create raw config parser so it can be set to read in keys, respecting case.
        self.configParser = configparser.RawConfigParser(dict_type=collections.OrderedDict)
        # Set case-sensitive parser by overriding the method that would oridinarily make the keys lowercase.
        self.configParser.optionxform = str

        self.configParser["GENERAL"] = collections.OrderedDict()
        self.configParser["OCR"] = collections.OrderedDict()
        self.configParser["FIELD REGEXES"] = collections.OrderedDict()
        self.configParser["AUTO REGEXES"] = collections.OrderedDict()
        self.configParser["RAW REGEXES"] = collections.OrderedDict()
        self.configParser["REGEX OPTIONS"] = collections.OrderedDict()

        self.configParser["GENERAL"]["Schema"] = ", ".join(self.schema)

        for option, val in self.ocrOptions.items():
            self.configParser['OCR'][option] = val

        for field, reg in self.fieldRegexes.items():
            self.configParser["FIELD REGEXES"][field] = reg

        for reg in self.regexes.values():
            if reg.isAuto:
                self.configParser["AUTO REGEXES"]["{}_name".format(reg.guid)] = reg.name
                self.configParser["AUTO REGEXES"]["{}_pattern".format(reg.guid)] = reg.pattern
                self.configParser["AUTO REGEXES"]["{}_flags".format(reg.guid)] = ", ".join(reg.flags)
            else:
                self.configParser["RAW REGEXES"]["{}_name".format(reg.guid)] = reg.name
                self.configParser["RAW REGEXES"]["{}_pattern".format(reg.guid)] = reg.pattern
                self.configParser["RAW REGEXES"]["{}_flags".format(reg.guid)] = ", ".join(reg.flags)

        for option, val in self.regexOptions.items():
            self.configParser['REGEX OPTIONS'][option] = val

        with open(file, 'w') as file:
            self.configParser.write(file)

    def RecompileAutoRegexes(self):
        """
        Recompiles all regexes that are automatically defined.
        :return:
        :rtype:
        """
        [regex.Compile() for regex in self.regexes.values() if regex.isAuto == True]